<?php

namespace UnicaenLeocarte\Service\Soap;

use SoapFault;
use Traversable;
use UnicaenLeocarte\Exception\RuntimeException;
use Zend\Soap\Client;
use Zend\Stdlib\ArrayUtils;

/**
 * Client SOAP capable d'appeler le Web Service Léocarte.
 *
 * @author Unicaen
 */
class SoapClient extends Client
{
    const SOAP_VERSION = SOAP_1_1;

    /*
     * operators
     */
    const OP_GREATER = 0;               // Greater
    const OP_LOWER = 1;                 // Lower
    const OP_EQUALS = 2;                // Equals
    const OP_NOT_EQUALS = 3;            // Not equals
    const OP_GREATER_OR_EQUALS = 4;     // Greater or equals
    const OP_LOWER_OR_EQUALS = 5;       // Lower or equals
    const OP_BETWEEN = 6;               // Between
    const OP_NOT_BETWEEN = 7;           // Not between
    const OP_NULL = 8;                  // Null
    const OP_NOT_NULL = 9;              // Not null
    const OP_UNIQUE = 10;               // Is unique
    const OP_NOT_UNIQUE = 11;           // Is not unique
    const OP_IN = 12;                   // Is in
    const OP_NOT_IN = 13;               // Is not in
    const OP_EMPTY = 14;                // Is empty
    const OP_NOT_EMPTY = 15;            // Is not empty
    const OP_LIKE = 16;                 // Like
    const OP_START_WITH = 17;           // Start with

    /*
     * types
     */
    const TY_STRING = 0;
    const TY_INTEGER = 1;
    const TY_DOUBLE = 2;
    const TY_DATETIME = 3;
    const TY_BOOLEAN = 4;
    const TY_IMAGE = 5;

    /*
     * datas
     */
    const D_STATUT = 955;                           // statut
    const D_DATE_FIN_VALIDITE = 969;                // date de fin de validité
    const D_DATE_DEBUT_VALIDITE = 970;              // date de début de validité
    const D_DROIT_PHOTO = 988;                      // droit à l'utilisation de la photo
    const D_PHOTO = 990;                            // photo recadrée
    const D_NOM = 994;                              // nom
    const D_PRENOM = 995;                           // prénom
    const D_REF_EXTERNE = 996;                      // référence externe : numéro étudiant / numéro personnel servant de référence dans l'application
    const D_STRUCTURE_PRINCIPALE_LIBELLE = 1021;    // libellé composante principale
    const D_STRUCTURE_PRINCIPALE_CODE = 1022;       // code composante principale
    const D_ETABLISSEMENT = 1026;                   // établissement
    const D_ETAPE_PRINCIPALE_LIBELLE = 1027;        // étape princiaple
    const D_ETAPE_PRINCIPALE_CODE = 1028;           // code étape principale
    const D_NOM_COMPLET = 1035;                     // nom prénom
    const D_CSN = 1044;                             // CSN
    const D_TYPE_INDIVIDU = 1047;                   // type d'individu
    const D_STRUCTURE_SECONDAIRE_LIBELLE = 1056;    // libellé composabnte secondaire
    const D_STRUCTURE_SECONDAIRE_CODE = 1057;       // code composante secondaire
    const D_UID = 1071;                             // UID
    const D_VET_PRINCIPALE_CODE = 1075;             // code version etape principale
    const D_VET_SECONDAIRE_CODE = 1076;             // code version etape secondaire
    const D_TYPE_POPULATION_LIBELLE = 1079;         // libellé type population
    const D_TYPE_POPULATION_CODE = 1080;            // code type population
    const D_LOGIN = 1085;                           // login


    /**
     *
     * @var array|Traversable $params
     */
    protected $params;


    public function __construct($params = [])
    {
        if (null !== $params) {
            $this->setParams($params);
        }

        $wsdlOptions = $this->params['wsdl'];
        $soapOptions = $this->params['soap'];

        // récupération du wsdl
        $file = sprintf('data://text/plain;base64,%s', base64_encode(file_get_contents($wsdlOptions['file'])));

        $options = [
            'soap_version' => isset($soapOptions['version']) ? $soapOptions['version'] : self::SOAP_VERSION,
            'cache_wsdl'   => 0,
            'proxy_host'   => isset($soapOptions['proxy_host']) ? $soapOptions['proxy_host'] : null,
            'proxy_port'   => isset($soapOptions['proxy_port']) ? $soapOptions['proxy_port'] : null,
        ];

        return parent::__construct($file ?: null, $options);
    }

    /**
     * Set Params
     *
     * @param  array|Traversable $params
     * @return SoapClient
     */
    public function setParams($params)
    {
        if ($params instanceof Traversable) {
            $params = ArrayUtils::iteratorToArray($params);
        }

        $this->params = $params;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    /**
     *
     * @param string $identifiant identifiant
     * @return mixed
     */
    public function getDroitUtilisationPhoto($identifiant)
    {
        $criteria = [
            'datumId'  => self::D_REF_EXTERNE,
            'operator' => self::OP_EQUALS,
            'valueMin' => ['stringValue' => $identifiant],
        ];
        $data = [
            self::D_DROIT_PHOTO,
        ];

        return $this->call('getDataValues', [$criteria, $data]);
    }

    /**
     *
     * @param string $identifiant identifiant
     * @return mixed
     */
    public function getPhoto($identifiant)
    {
        $criteria = [
            'datumId'  => self::D_REF_EXTERNE,
            'operator' => self::OP_EQUALS,
            'valueMin' => ['stringValue' => $identifiant],
        ];
        $data = [
            self::D_PHOTO,
        ];

        return $this->call('getDataValues', [$criteria, $data]);
    }

    /**
     * Méthode magique __call
     *
     * @param string $name      nom de la méthode
     * @param array  $arguments arguments de la méthode
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }

        $args = ['arg0' => $this->params['wsdl']['username'], 'arg1' => $this->params['wsdl']['password']];

        if (!empty($arguments[0]))
            $args['arg2'] = $arguments[0];

        if (!empty($arguments[1]))
            $args['arg3'] = $arguments[1];

        $params[] = $args;

        try {
            return parent::__call($name, $params);
        }
        catch (SoapFault $e) {
            throw new RuntimeException("Erreur lors de l'appel SOAP : " . $e->getMessage(), 0, $e);
        }
    }

    /**
     * Send an RPC request to the service for a specific method.
     *
     * @param  string $method nom de la méthode
     * @param  array  $params liste des paramètres
     * @return mixed
     */
    public function call($method, $params = [])
    {
        return call_user_func_array([$this, '__call'], [$method, $params]);
    }
}