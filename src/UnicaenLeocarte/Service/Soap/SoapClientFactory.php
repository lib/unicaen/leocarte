<?php

namespace UnicaenLeocarte\Service\Soap;

use Interop\Container\ContainerInterface;
use UnicaenLeocarte\Options\ModuleOptions;

/**
 * Leocarte Soap Client factory.
 *
 * @author Unicaen
 */
class SoapClientFactory
{
    public function __invoke(ContainerInterface $serviceLocator)
    {
        /** @var ModuleOptions $options */
        $options = $serviceLocator->get('UnicaenLeocarte\Options');

        return new SoapClient($options->getSoapClientConfig());
    }
}