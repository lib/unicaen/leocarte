<?php

namespace UnicaenLeocarte\Service\Photo;

trait PhotoServiceAwareTrait
{
    /**
     * @var PhotoService
     */
    protected $photoService;

    /**
     * @param PhotoService $service
     */
    public function setPhotoService(PhotoService $service)
    {
        $this->photoService = $service;
    }
}