<?php

namespace UnicaenLeocarte;

use UnicaenLeocarte\Controller\IndexControllerFactory;
use UnicaenLeocarte\Options\ModuleOptionsFactory;
use UnicaenLeocarte\Service\Photo\PhotoServiceFactory;
use UnicaenLeocarte\Service\Soap\SoapClientFactory;

return [
    'bjyauthorize'    => [
        'guards' => [
            'UnicaenAuth\Guard\PrivilegeController' => [
                [
                    'controller' => 'UnicaenLeocarte\Controller\Index',
                    'action'     => [
                        'photo',
                    ],
//                    'privileges' => \UnicaenLeocarte\Provider\Privilege\ThesePrivileges::THESE_RECHERCHE,
                    'roles' => ['user'],
                ],
            ],
        ],
    ],
    'router'          => [
        'routes' => [
            'leocarte' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/leocarte',
                    'defaults' => [
                        'controller' => 'UnicaenLeocarte\Controller\Index',
                    ],
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'photo' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/photo/:id',
                            'constraints' => [
                                'id' => '[0-9]+',
                            ],
                            'defaults'    => [
                                'action' => 'photo',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'UnicaenLeocarte\Service\Photo' => PhotoServiceFactory::class,
            'UnicaenLeocarte\Options'       => ModuleOptionsFactory::class,
            'UnicaenLeocarte\Soap\Client'   => SoapClientFactory::class,
        ],
    ],

    'controllers'  => [
        'factories' => [
            'UnicaenLeocarte\Controller\Index' => IndexControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];