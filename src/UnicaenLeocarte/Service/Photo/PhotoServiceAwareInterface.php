<?php

namespace UnicaenLeocarte\Service\Photo;

interface PhotoServiceAwareInterface
{
    public function setPhotoService(PhotoService $service);
}