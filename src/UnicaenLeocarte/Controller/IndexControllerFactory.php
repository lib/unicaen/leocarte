<?php

namespace UnicaenLeocarte\Controller;

use Interop\Container\ContainerInterface;
use UnicaenLeocarte\Options\ModuleOptions;

/**
 * IndexController factory.
 *
 * @author Unicaen
 */
class IndexControllerFactory
{
    function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $options */
        $options = $container->get('UnicaenLeocarte\Options');

        /** @var \UnicaenLeocarte\Service\Photo\PhotoService $photoService */
        $photoService = $container->get('UnicaenLeocarte\Service\Photo');

        $controller = new IndexController();
        $controller->setPhotoService($photoService);
        $controller->setCheckDroitUtilisationPhoto($options->getPhotoConfig()['check_droit_utilisation_photo']);

        return $controller;
    }
}