<?php

namespace UnicaenLeocarte\Options;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Exception\InvalidArgumentException;

/**
 * Module options factory.
 *
 * @author Unicaen
 */
class ModuleOptionsFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('Configuration');

        $key = 'unicaen-leocarte';

        if (! isset($config[$key])) {
            throw new InvalidArgumentException("Configuration du module UnicaenLeocarte introuvable (clé '$key')");
        }

        return new ModuleOptions($config[$key]);
    }
}