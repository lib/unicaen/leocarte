<?php

namespace UnicaenLeocarte\Service\Photo;

use Interop\Container\ContainerInterface;
use UnicaenLeocarte\Options\ModuleOptions;
use UnicaenLeocarte\Service\Soap\SoapClient;

/**
 * Leocarte Service factory.
 *
 * @author Unicaen
 */
class PhotoServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $options */
        $options = $container->get('UnicaenLeocarte\Options');

        /** @var SoapClient $soapClient */
        $soapClient = $container->get('UnicaenLeocarte\Soap\Client');

        $service = new PhotoService();
        $service->setSoapClient($soapClient);
        $service->setGenerationPhotoNonAutoriseeConfig($options->getPhotoConfig()['generation_photo_non_autorisee_config']);

        return $service;
    }
}