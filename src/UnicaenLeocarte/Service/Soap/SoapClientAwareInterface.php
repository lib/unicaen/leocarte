<?php

namespace UnicaenLeocarte\Service\Soap;

interface SoapClientAwareInterface
{
    public function setSoapClient(SoapClient $service);
}