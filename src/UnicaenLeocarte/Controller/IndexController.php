<?php

namespace UnicaenLeocarte\Controller;

use UnicaenLeocarte\Exception\ExtensionNotLoadedException;
use UnicaenLeocarte\Exception\NotFoundException;
use UnicaenLeocarte\Exception\RuntimeException;
use UnicaenLeocarte\Service\Photo\PhotoServiceAwareInterface;
use UnicaenLeocarte\Service\Photo\PhotoServiceAwareTrait;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController implements PhotoServiceAwareInterface
{
    use PhotoServiceAwareTrait;

    /**
     * @var bool Témoin indiquant si le droit d'utilisation de la photo doit être pris en considération ou non.
     */
    protected $checkDroitUtilisationPhoto = true;

    /**
     * Récupère la photo d'un étudiant.
     *
     * NB: Si le droit d'utilisation de celle-ci n'est pas accordé, une image informative générée est retournée
     * (si l'extension PHP 'gd' est activée).
     *
     * @returns Response Réponse HTTP (par défaut, l'autorisation de mise en cache par le client est activée)
     */
    public function photoAction()
    {
        $id = $this->params()->fromRoute('id');
        $nocache = (bool) (int) $this->params()->fromQuery('nocache', '0');
        $failure = false;

        try {
            $autorise = true;
            if ($this->checkDroitUtilisationPhoto) {
                $autorise = $this->photoService->getDroitUtilisationPhoto($id);
            }

            if ($autorise) {
                $content = $this->photoService->getPhoto($id);
            }
            else {
                try {
                    $content = $this->photoService->createPhotoNonAutorisee();
                }
                catch (ExtensionNotLoadedException $e) {
                    // Extension 'gd' non installée
                    $content = '';
                    $failure = true;
                }
            }
        }
        catch (NotFoundException $nfe) {
            // Requête infructueuse.
            // On tente de fabriquer une image informative...
            try {
                $content = $this->photoService->createPhotoIntrouvable();
            }
            catch (ExtensionNotLoadedException $enle) {
                // Extension 'gd' non installée
                $content = '';
            }
            $failure = true;
        }
        catch (RuntimeException $re) {
            // Erreur lors de l'appel du WS
            error_log("Erreur rencontrée lors de l'appel au web service Leocarte : " . $re->getMessage(), 0);
            $content = $this->photoService->createPhotoErreur();
            $failure = true;
        }

        /** @var Response $response */
        $response = $this->getResponse();
        $response->setContent($content);

        $headers = $response->getHeaders();
        $headers
            ->addHeaderLine('Content-Transfer-Encoding', "binary")
            ->addHeaderLine('Content-Type', "image/jpeg")
            ->addHeaderLine('Content-length', strlen($content));

        if ($nocache || $failure) {
            $headers
                ->addHeaderLine('Cache-Control', "no-cache")
                ->addHeaderLine('Pragma', 'no-cache');
        }
        else {
            // autorisation de la mise en cache de l'image par le client
            $maxAge = 60 * 60 * 24; // 86400 secondes = 1 jour
            $headers
                ->addHeaderLine('Cache-Control', "private, max-age=$maxAge")
                ->addHeaderLine('Pragma', 'private')// tout sauf 'no-cache'
                ->addHeaderLine('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + $maxAge));
        }

        return $response;
    }

    /**
     * Spécifie s'il faut vérifier que l'utilisation de la photo est autorisée ou non.
     *
     * @param bool $check
     */
    public function setCheckDroitUtilisationPhoto($check = true)
    {
        $this->checkDroitUtilisationPhoto = $check;
    }
}