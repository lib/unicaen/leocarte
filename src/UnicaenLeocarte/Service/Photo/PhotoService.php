<?php

namespace UnicaenLeocarte\Service\Photo;

use UnicaenApp\Util;
use UnicaenLeocarte\Exception\NotFoundException;
use UnicaenLeocarte\Service\Soap\SoapClientAwareInterface;
use UnicaenLeocarte\Service\Soap\SoapClientAwareTrait;

/**
 * Guichet pour les opérations concernant les photos.
 *
 * @author Unicaen
 */
class PhotoService implements SoapClientAwareInterface
{
    use SoapClientAwareTrait;

    const UTILISATION_PHOTO_AUTORISEE = 3;

    /**
     * @var array
     */
    protected $generationPhotoNonAutoriseeConfig;

    /**
     * Retourne le témoin de droit d'utilisation de la photo pour l'identifiant spécifié.
     *
     * @param string $identifiant Identifiant, ex: numéro étudiant
     * @return boolean
     */
    public function getDroitUtilisationPhoto($identifiant)
    {
        $result = $this->soapClient->getDroitUtilisationPhoto($identifiant);

        if (! isset($result->return->resources)) {
            throw new NotFoundException("Aucune ressource trouvée");
        }

        $resource = $this->extractDroitUtilisationPhotoResource($result);

        if ($resource === null) {
            throw new NotFoundException("Aucun droit photo trouvé");
        }

        $value = $resource->value->integerValue;

        return $value === self::UTILISATION_PHOTO_AUTORISEE;
    }

    /**
     * @param object $result
     * @return object|null
     */
    protected function extractDroitUtilisationPhotoResource($result)
    {
        $resources = (array) $result->return->resources;

        return array_shift($resources);
    }

    /**
     * Retourne la photo pour l'identifiant spécifié.
     *
     * @param string $identifiant Identifiant, ex: numéro étudiant
     * @return string Contenu "binaire" de la photo
     */
    public function getPhoto($identifiant)
    {
        /** @var \stdClass $result */
        $result =  $this->soapClient->getPhoto($identifiant);

        if (! isset($result->return->resources)) {
            throw new NotFoundException("Aucune ressource trouvée");
        }

        $resource = $this->extractPhotoResource($result);

        if ($resource === null) {
            throw new NotFoundException("Aucune photo trouvée");
        }

        // c'est le contenu "binaire" de la photo qui est retourné par le WS
        $value = $resource->value->imageValue;

        return $value;
    }

    /**
     * @param object $result
     * @return object|null
     */
    protected function extractPhotoResource($result)
    {
        $resources = (array) $result->return->resources;

        $found = null;
        foreach ($resources as $resource) {
            if (isset($resource->id) && $resource->id > 0) {
                $found = $resource;
                break;
            }
        }

        return $found;
    }

    /**
     * Fabrique une image informant d'une erreur lors de l'appel au WS.
     *
     * @return string Contenu binaire de l'image générée
     */
    public function createPhotoErreur()
    {
        return $this->_createImage(
            "            |" .
            " Une erreur |" .
            "     est    |" .
            "   survenue ");
    }

    /**
     * Fabrique une image informant que la photo est introuvable.
     *
     * @return string Contenu binaire de l'image générée
     */
    public function createPhotoIntrouvable()
    {
        return $this->_createImage(
            "            |" .
            "            |" .
            "    Photo   |" .
            " introuvable");
    }

    /**
     * Fabrique une image informant que l'utilisation de la photo n'est pas autorisée.
     *
     * @return string Contenu binaire de l'image générée
     */
    public function createPhotoNonAutorisee()
    {
        return $this->_createImage(
            "             |" .
            " Utilisation |" .
            " de la photo |" .
            "non autorisée"
        );
    }

    /**
     * @param string $texte Texte avec des pipes à l'emplacement des retour à la ligne,
     *                      ex: "Utilisation | de la photo | non autorisée"
     * @return string
     */
    private function _createImage($texte)
    {
        $width  = $this->generationPhotoNonAutoriseeConfig['width'];
        $height = $this->generationPhotoNonAutoriseeConfig['height'];

        return Util::createImageWithText($texte, $width, $height);
    }

    /**
     * Spécifie la config de génération de l'image inforamtive en cas de d'interdiction d'utilisation de la photo.
     *
     * @param array $config
     * @return $this
     */
    public function setGenerationPhotoNonAutoriseeConfig(array $config)
    {
        $this->generationPhotoNonAutoriseeConfig = $config;

        return $this;
    }
}