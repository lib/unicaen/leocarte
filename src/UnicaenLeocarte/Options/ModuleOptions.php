<?php

namespace UnicaenLeocarte\Options;

use Zend\Stdlib\AbstractOptions;
use Zend\Stdlib\ArrayUtils;

/**
 * Classe encapsulant les options du module UnicaenLeocarte.
 *
 * @author Unicaen
 */
class ModuleOptions extends AbstractOptions
{
    /**
     * Config du client SOAP.
     *
     * @var array
     */
    protected $soapClientConfig = [
        'wsdl' => [
            'file'     => '',
            'username' => '',
            'password' => '',

        ],
        'soap' => [
            'version' => SOAP_1_1, // cf. extension "php-soap"
        ],
    ];

    /**
     * Options concernant les photos.
     *
     * @var array
     */
    protected $photoConfig = [
        /**
         * Témoin indiquant si le droit d'utilisation de la photo doit être pris en considération ou non.
         */
        'check_droit_utilisation_photo' => true,

        /**
         * Options de génération d'une image informative lorsque le droit d'utilisation de la photo est refusé.
         */
        'generation_photo_non_autorisee_config' => [
            'width'   => 100,
            'height'  => 120,
        ],
    ];

    /**
     * Retourne la config du client SOAP.
     *
     * @return array
     */
    public function getSoapClientConfig()
    {
        return $this->soapClientConfig;
    }

    /**
     * Fusionne la config courante du client SOAP avec celle spécifiée.
     *
     * @param array $config
     * @return self
     */
    public function setSoapClientConfig(array $config = [])
    {
        $this->soapClientConfig = ArrayUtils::merge($this->soapClientConfig, $config);

        return $this;
    }

    /**
     * @return array
     */
    public function getPhotoConfig()
    {
        return $this->photoConfig;
    }

    /**
     * Fusionne la config courante concernant les photos avec celle spécifiée.
     *
     * @param array $config
     * @return $this
     */
    public function setPhotoConfig($config)
    {
        $this->photoConfig = ArrayUtils::merge($this->photoConfig, $config);

        return $this;
    }
}